// Usage: node json_parse.js [--print-as-array] arg1 arg2
// Parse json string arg1 and return it's arg2 property.
// If --print-as-array is used, the resulted property is
// treated as array and each member is returned in new line,
// that is useful for sh loop over members.
try {
	var arg = process.argv.slice(2);
	var print_as_array = false;
	if (arg[0] === "--print-as-array") {
		arg.shift();
		print_as_array = true;
	}
	var result = JSON.parse(arg[0])[arg[1]];
	if (print_as_array && Array.isArray(result)) {
		for (var i=0; i < result.length; i++) {
			console.log(result[i]);
		}
	}
	else {
		console.log(JSON.stringify(result));
	}
}
catch (err) {
	console.log("undefined");
	process.exit(1);
}

